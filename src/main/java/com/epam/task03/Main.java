package com.epam.task03;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ArrayTasks arrayTasks = new ArrayTasks();
        int[] f = new int[] {
                0,
                3,
                2,
                4,
                6
        };
        int[] s = new int[] {
                1,
                2,
                4,
                5,
                6
        };
        int[] duplicates = new int[] {
                2,
                2,
                3,
                3,
                3,
                4,
                4,
                5
        };

        int[] sequences = new int[] {
                2,
                2,
                3,
                4,
                3,
                1,
                1,
                1
        };


        System.out.println("Intersection of two arrays:");
        Arrays.stream(arrayTasks.intersection(f, s)).forEach(System.out::println);

        System.out.println("Unique elements of first array");
        Arrays.stream(arrayTasks.difference(f, s)).forEach(System.out::println);

        System.out.println("Remove number that appears more than 2 times");
        Arrays.stream(arrayTasks.removeAllRepeatMoreThanTwo(duplicates)).forEach(System.out::println);

        System.out.println("Remove all sequence elements except one");
        Arrays.stream(arrayTasks.arraySequence(sequences)).forEach(System.out::println);
    }
}
