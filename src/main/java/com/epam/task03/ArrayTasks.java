package com.epam.task03;

import java.util.*;

public class ArrayTasks {
    public int[] intersection(int[] firstArray, int[] secondArray) {
        return Arrays
                .stream(firstArray)
                .distinct()
                .filter(x -> Arrays.stream(secondArray)
                        .anyMatch(y -> y == x))
                .toArray();
    }

    public int[] difference(int[] firstArray, int[] secondArray) {
        return Arrays
                .stream(firstArray)
                .distinct()
                .filter(x -> Arrays.stream(secondArray)
                        .noneMatch(y -> y == x))
                .toArray();
    }

    public int[] removeAllRepeatMoreThanTwo(int[] array) {
        Map<Integer, Integer> map = new HashMap<>();
        int number;
        for (int i : array) {
            int count = 0;
            number = i;
            for (int j : array) {
                if (number == j) count++;
            }
            map.put(number, count);
        }
        List<Integer> list = new ArrayList<>();
        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            if (e.getValue() <= 2) {
                for (int i = 0; i < e.getValue(); ++i) {
                    list.add(e.getKey());
                }
            }
        }

        int[] result = new int[list.size()];

        for (int i = 0; i < result.length; ++i) {
            result[i] = list.get(i);
        }

        return result;
    }

    public int[] arraySequence(int[] array) {
        List<Integer> list = new ArrayList<>();
        int[] result;

        int curr = 0;
        for (int i : array) {
            int sequenceLength = 0;
            if (curr == i) {
                sequenceLength++;
            } else {
                list.add(i);
            }
            if (sequenceLength > 1) {
                list.add(i);
            }
            curr = i;
        }

        result = new int[list.size()];
        for (int i = 0; i < result.length; ++i) {
            result[i] = list.get(i);
        }
        return result;
    }
}
