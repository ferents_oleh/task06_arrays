package com.epam.task04.view;

import com.epam.task04.controller.GameController;
import com.epam.task04.model.domain.Hero;
import com.epam.task04.model.domain.Room;

import java.util.List;

public class ConsoleOutput {
    private GameController gameController;

    private List<Room> rooms;

    public ConsoleOutput(GameController gameController) {
        this.gameController = gameController;
        printRoomInfo();
        proceedFight();
        printCountRoomsWithDeaths();
        printBestRoomSequence();
    }

    private void printRoomInfo() {
        rooms = gameController.generateRooms();
        System.out.println("Rooms information");
        for (Room room : rooms) {
            System.out.print(room.getRoomId() + "\t");
            if (room.isHasEnemy()) {
                System.out.print("Enemy\t");
                System.out.print(room.getEnemy().getDamage());
            } else {
                System.out.print("Artifact\t");
                System.out.print(room.getArtifact().getPoints());
            }
            System.out.println();
        }
    }

    private void proceedFight() {
        gameController.fight(rooms);
    }

    private void printCountRoomsWithDeaths() {
        System.out.println("Rooms with deaths:");
        System.out.println(gameController.calculateRoomsWithDeaths(rooms));
    }

    private void printBestRoomSequence() {
        System.out.println("Finding best room sequence...");
        gameController.findBestRoomSequence(rooms).forEach(System.out::println);
    }
}
