package com.epam.task04.controller.impl;

import com.epam.task04.controller.GameController;
import com.epam.task04.model.dao.ArtifactDao;
import com.epam.task04.model.dao.EnemyDao;
import com.epam.task04.model.dao.RoomDao;
import com.epam.task04.model.domain.Hero;
import com.epam.task04.model.domain.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameControllerImpl implements GameController {
    private RoomDao roomDao;

    private EnemyDao enemyDao;

    private ArtifactDao artifactDao;

    private int deathsCount;

    public GameControllerImpl(RoomDao roomDao, EnemyDao enemyDao, ArtifactDao artifactDao) {
        this.roomDao = roomDao;
        this.enemyDao = enemyDao;
        this.artifactDao = artifactDao;
    }

    @Override
    public List<Room> generateRooms() {
        Random random = new Random();
        List<Room> rooms = new ArrayList<>();
        for (int i = 0; i < 10; ++i) {
            rooms.add(roomDao.createRoom(i, random.nextBoolean()));
        }
        for (Room room : rooms) {
            if (room.isHasEnemy()) {
                room.setEnemy(enemyDao.createEnemy(random.nextInt(100 - 5) + 5));
            } else {
                room.setArtifact(artifactDao.createArtifact(random.nextInt(80 - 10) + 10));
            }
        }
        return rooms;
    }

    @Override
    public void fight(List<Room> rooms) {
        Hero hero = new Hero(25);
        for (Room room : rooms) {
            System.out.println("Room №" + room.getRoomId());
            if (room.isHasEnemy()) {
                if (hero.getDamage() < room.getEnemy().getDamage()) {
                    System.out.println("Hero is dead");
                    break;
                }
            } else {
                hero.setDamage(room.getArtifact().getPoints());
            }
        }
    }

    @Override
    public int calculateRoomsWithDeaths(List<Room> rooms) {
        Hero hero = new Hero(25);
        for (Room room : rooms) {
            if (room.isHasEnemy()) {
                if (hero.getDamage() < room.getEnemy().getDamage()) {
                    deathsCount++;
                }
            }
        }
        return deathsCount;
    }

    @Override
    public List<Long> findBestRoomSequence(List<Room> rooms) {
        Hero hero = new Hero(25);
        List<Long> result = new ArrayList<>();
        for (Room room : rooms) {
            if (!room.isHasEnemy()) {
                result.add(room.getRoomId());
                hero.setDamage(room.getArtifact().getPoints());
            } else {
                if (!(hero.getDamage() < room.getEnemy().getDamage())) {
                    result.add(room.getRoomId());
                }
            }
        }
        return result;
    }
}
