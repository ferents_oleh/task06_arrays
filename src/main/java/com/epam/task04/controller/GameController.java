package com.epam.task04.controller;

import com.epam.task04.model.domain.Room;

import java.util.List;

public interface GameController {
    List<Room> generateRooms();

    void fight(List<Room> rooms);

    int calculateRoomsWithDeaths(List<Room> rooms);

    List<Long> findBestRoomSequence(List<Room> rooms);
}
