package com.epam.task04;

import com.epam.task04.controller.GameController;
import com.epam.task04.controller.impl.GameControllerImpl;
import com.epam.task04.model.dao.ArtifactDao;
import com.epam.task04.model.dao.EnemyDao;
import com.epam.task04.model.dao.RoomDao;
import com.epam.task04.model.dao.impl.ArtifactDaoImpl;
import com.epam.task04.model.dao.impl.EnemyDaoImpl;
import com.epam.task04.model.dao.impl.RoomDaoImpl;
import com.epam.task04.view.ConsoleOutput;

public class Main {
    public static void main(String[] args) {
        RoomDao roomDao = new RoomDaoImpl();
        EnemyDao enemyDao = new EnemyDaoImpl();
        ArtifactDao artifactDao = new ArtifactDaoImpl();
        GameController gameController = new GameControllerImpl(roomDao, enemyDao, artifactDao);
        new ConsoleOutput(gameController);
    }
}
