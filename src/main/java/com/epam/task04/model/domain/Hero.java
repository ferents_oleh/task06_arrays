package com.epam.task04.model.domain;

public class Hero {
    private int damage;

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Hero(int damage) {
        this.damage = damage;
    }
}
