package com.epam.task04.model.domain;

public class Room {
    private long roomId;

    private Enemy enemy;

    private Artifact artifact;

    private boolean hasEnemy;

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    public void setArtifact(Artifact artifact) {
        this.artifact = artifact;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public Artifact getArtifact() {
        return artifact;
    }

    public boolean isHasEnemy() {
        return hasEnemy;
    }

    public void setHasEnemy(boolean hasEnemy) {
        this.hasEnemy = hasEnemy;
    }

    public Room(long roomId, boolean hasEnemy) {
        this.roomId = roomId;
        this.hasEnemy = hasEnemy;
    }


}
