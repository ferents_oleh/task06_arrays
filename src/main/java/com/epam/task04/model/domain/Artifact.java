package com.epam.task04.model.domain;

public class Artifact {
    private int points;

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Artifact(int points) {
        this.points = points;
    }
}
