package com.epam.task04.model.domain;

public class Enemy {
    private int damage;

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Enemy(int damage) {
        this.damage = damage;
    }
}
