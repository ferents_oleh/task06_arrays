package com.epam.task04.model.dao;

import com.epam.task04.model.domain.Artifact;

public interface ArtifactDao {
    Artifact createArtifact(int points);
}
