package com.epam.task04.model.dao.impl;

import com.epam.task04.model.dao.EnemyDao;
import com.epam.task04.model.domain.Enemy;

public class EnemyDaoImpl implements EnemyDao {
    @Override
    public Enemy createEnemy(int points) {
        return new Enemy(points);
    }
}
