package com.epam.task04.model.dao.impl;

import com.epam.task04.model.dao.RoomDao;
import com.epam.task04.model.domain.Room;

public class RoomDaoImpl implements RoomDao {
    @Override
    public Room createRoom(long id, boolean hasEnemy) {
        return new Room(id, hasEnemy);
    }
}
