package com.epam.task04.model.dao;

import com.epam.task04.model.domain.Room;

public interface RoomDao {
    Room createRoom(long id, boolean hasEnemy);
}
