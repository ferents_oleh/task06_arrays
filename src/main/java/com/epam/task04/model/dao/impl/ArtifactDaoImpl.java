package com.epam.task04.model.dao.impl;

import com.epam.task04.model.dao.ArtifactDao;
import com.epam.task04.model.domain.Artifact;

public class ArtifactDaoImpl implements ArtifactDao {
    @Override
    public Artifact createArtifact(int points) {
        return new Artifact(points);
    }
}
