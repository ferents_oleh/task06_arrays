package com.epam.task04.model.dao;

import com.epam.task04.model.domain.Enemy;

public interface EnemyDao {
    Enemy createEnemy(int points);
}
