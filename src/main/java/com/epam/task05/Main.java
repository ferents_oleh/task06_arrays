package com.epam.task05;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        StringArrayContainer stringArrayContainer = new StringArrayContainer();
        stringArrayContainer.add("first");
        stringArrayContainer.add("second");
        stringArrayContainer.add("third");
        Arrays.stream(stringArrayContainer.getArray()).forEach(System.out::println);
        stringArrayContainer.add("fourth");
        Arrays.stream(stringArrayContainer.getArray()).forEach(System.out::println);
        stringArrayContainer.add("fifth");
        Arrays.stream(stringArrayContainer.getArray()).forEach(System.out::println);

    }
}
