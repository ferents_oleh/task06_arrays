package com.epam.task05;

import java.util.Arrays;

public class StringArrayContainer {
    private String[] array;

    public StringArrayContainer() {
        array = new String[1];
    }

    public String[] getArray() {
        return array;
    }

    public void add(String str) {
        if (array[0] != null) {
            String[] temp = Arrays.copyOf(array, array.length + 1);
            temp[array.length] = str;
            array = temp;
        } else {
            array[0] = str;
        }
    }

    public String get(int index) {
        if (index > array.length) {
            return "";
        }
        return array[index];
    }

    public int size() {
        return array.length;
    }
}
