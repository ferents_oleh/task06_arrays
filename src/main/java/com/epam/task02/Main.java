package com.epam.task02;

import com.epam.task01.Camera;
import com.epam.task01.Phone;
import com.epam.task01.Product;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Product> products = new ArrayList<>() {
            {
                add(new Phone("samsung", 6999, 2017));
                add(new Camera("canon", 25000, 2016));
                add(new Phone("xiaomi", 3500, 2015));
                add(new Phone("huawei", 3500, 2012));
            }
        };
        CustomPriorityQueue<Product> customPriorityQueue = new CustomPriorityQueue<>(new ProductComparator());
        customPriorityQueue.addAll(products);
        Iterator it = customPriorityQueue.iterator();
        while(it.hasNext()) {
            System.out.println(customPriorityQueue.poll().getName());
        }
    }
    static class ProductComparator implements Comparator<Product> {
        @Override
        public int compare(Product o1, Product o2) {
            if (o1.getReleaseYear() < o2.getReleaseYear()) {
                return -1;
            } else if(o1.getReleaseYear() > o2.getReleaseYear()) {
                return 1;
            }
            return 0;
        }
    }
}
