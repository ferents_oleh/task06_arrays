package com.epam.task07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        CustomDeque<Integer> customDeque = new CustomDeque<>();
        customDeque.add(123);
        customDeque.add(456);
        customDeque.add(456);
        customDeque.add(789);
        customDeque.add(456);
        customDeque.forEach(System.out::println);
//        System.out.println("--------");
//        List<Integer> list = new ArrayList<>() {
//            {
//                add(456);
//            }
//        };
//        customDeque.retainAll(list);
//        customDeque.forEach(System.out::println);
        System.out.println("--------");
        customDeque.forEach(System.out::println);
        System.out.println(customDeque.containsAll(Arrays.asList(123, 456, 1111)));
//        System.out.println(customDeque.contains(456));
    }
}
