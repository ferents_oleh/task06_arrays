package com.epam.task07;

import java.util.*;

public class CustomDeque<E> implements Deque<E> {
    private E[] array = (E[]) new Object[1];
    ;

    @Override
    public void addFirst(E e) {
        offerLast(e);
    }

    @Override
    public void addLast(E e) {
        offerLast(e);
    }

    @Override
    public boolean offerFirst(E e) {
        if (e == null) {
            return false;
        }
        E[] temp = (E[]) new Object[size() + 1];
        if (size() - 1 >= 0) System.arraycopy(array, 1, temp, 1, size() - 1);
        temp[0] = e;
        array = temp;
        return true;
    }

    @Override
    public boolean offerLast(E e) {
        if (e == null) {
            return false;
        }
        if (array[0] != null) {
            E[] temp = (E[]) new Object[size() + 1];
            System.arraycopy(array, 0, temp, 0, size());
            temp[size()] = e;
            array = temp;
        } else {
            array[0] = e;
        }
        return true;
    }

    @Override
    public E removeFirst() {
        return pollFirst();
    }

    @Override
    public E removeLast() {
        return pollLast();
    }

    @Override
    public E pollFirst() {
        E element = array[0];

        E[] temp = (E[]) new Object[size() - 1];

        System.arraycopy(array, 1, temp, 0, size() - 1);
        array = temp;

        return element;
    }

    @Override
    public E pollLast() {
        E element = array[size() - 1];

        E[] temp = (E[]) new Object[size() - 1];

        System.arraycopy(array, 0, temp, 0, size() - 1);

        array = temp;

        return element;
    }

    @Override
    public E getFirst() {
        return peekFirst();
    }

    @Override
    public E getLast() {
        return peekLast();
    }

    @Override
    public E peekFirst() {
        return array[0];
    }

    @Override
    public E peekLast() {
        return array[size() - 1];
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        if (o == null) {
            return false;
        }

        E[] temp = (E[]) new Object[size() - 1];

        for (int i = 0; i < size() - 1; ++i) {
            if (array[i].equals(o)) {
                for (int j = 1; j < size() - 1; ++j) {
                    array[j] = array[j + 1];
                }
                array[size() - 1] = null;
            }
        }
        for (int i = 0; i < temp.length; ++i) {
            if (array[i] != null) {
                temp[i] = array[i];
            }
        }
        array = temp;
        return true;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        if (o == null) {
            return false;
        }

        E[] temp = (E[]) new Object[size() - 1];
        int index = 0;
        for (int i = size() - 1; i >= 0; --i) {
            if (array[i].equals(o)) {
                index = i;
                break;
            }
        }
        for (int i = 0; i < size(); ++i) {
            if (i == index) {
                array[i] = null;
            }
        }
        for (int i = 0; i < temp.length; ++i) {
            temp[i] = array[i];
        }
        array = temp;
        return true;
    }

    @Override
    public boolean add(E e) {
        if (e == null) {
            return false;
        }
        addLast(e);
        return true;
    }

    @Override
    public boolean offer(E e) {
        return add(e);
    }

    @Override
    public E remove() {
        return removeFirst();
    }

    @Override
    public E poll() {
        return pollFirst();
    }

    @Override
    public E element() {
        return getFirst();
    }

    @Override
    public E peek() {
        return getFirst();
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() == null) {
                return false;
            }
            addLast((E) iterator.next());
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() == null) {
                return false;
            }
            remove((E) iterator.next());
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (int j = 0; j < c.size(); ++j) {
            for (int i = 0; i < size(); ++i) {
                E obj = array[i];
                if (!c.contains(obj)) {
                    remove(array[i]);
                }
            }
        }
        return true;
    }

    @Override
    public void clear() {
        Arrays.fill(array, null);
    }

    @Override
    public void push(E e) {
        addFirst(e);
    }

    @Override
    public E pop() {
        return removeFirst();
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            return false;
        }

        for (int i = 0; i < size(); ++i) {
            if (array[i].equals(o)) {
                array = removeByIndex(i);
            }
        }

        return true;
    }

    private E[] removeByIndex(int index) {
        E[] newArr = (E[]) new Object[size() - 1];
        for (int i = 0, k = 0; i < size(); ++i) {
            if (i == index) {
                continue;
            }
            newArr[k++] = array[i];
        }
        return newArr;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        if (c == null) {
            return false;
        }

        for (var el : c) {
            if (!contains(el)) {
                return false;
            } else {
                continue;
            }
        }
        return true;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) {
            return false;
        }

        for (int i = 0; i < size(); ++i) {
            if (array[i].equals(o)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new DeqIterator();
    }

    @Override
    public Object[] toArray() {
        Object[] temp = new Object[size()];
        for (int i = 0; i < size(); i++) {
            temp[i] = array[i];
        }
        return temp;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        for (int i = 0; i < size(); i++) {
            a[i] = (T) array[i];
        }
        return a;
    }

    @Override
    public Iterator<E> descendingIterator() {
        return new ReverseDeqIterator();
    }

    private class DeqIterator implements Iterator<E> {
        private int position = 0;

        @Override
        public boolean hasNext() {
            return position < size();
        }

        @Override
        public E next() {
            if (hasNext()) {
                return array[position++];
            } else {
                return null;
            }
        }
    }

    private class ReverseDeqIterator implements Iterator<E> {
        private int position = size() - 1;

        @Override
        public boolean hasNext() {
            return position > size();
        }

        @Override
        public E next() {
            if (hasNext()) {
                return array[position--];
            } else {
                return null;
            }
        }
    }
}
