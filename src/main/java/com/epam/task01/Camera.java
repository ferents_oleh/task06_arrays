package com.epam.task01;

public class Camera extends Product {
    public Camera(String name, int price, int releaseYear) {
        super(name, price, releaseYear);
    }

    @Override
    public String toString() {
        return "Camera{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", releaseYear=" + releaseYear +
                '}';
    }
}
