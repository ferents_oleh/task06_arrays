package com.epam.task01;

public class Phone extends Product {
    public Phone(String name, int price, int releaseYear) {
        super(name, price, releaseYear);
    }

    @Override
    public String toString() {
        return "Phone{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", releaseYear=" + releaseYear +
                '}';
    }
}
