package com.epam.task01;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ProductContainer<Product> productContainer = new ProductContainer<>();
        productContainer.add(new Camera("test", 2000, 2005));
        productContainer.add(new Phone("samsung", 10000, 2019));
        List<Product> products = new ArrayList<>() {
            {
                add(new Camera("canon", 25000, 2016));
                add(new Phone("xiaomi", 3500, 2015));
                add(new Phone("huawei", 3500, 2012));
            }
        };
        productContainer.addAll(products);
        productContainer.printAll(productContainer.getAll());
    }
}
