package com.epam.task01;

import java.util.ArrayList;
import java.util.List;

public class ProductContainer <T extends Product> {
    private List<? super Product> productList = new ArrayList<>();

    public void add(T product) {
        productList.add(product);
    }

    public void remove(T product) {
        productList.remove(product);
    }

    public void addAll(List<? extends T> list) {
        productList.addAll(list);
    }

    public List<? super T> getAll() {
        return productList;
    }

    public void printAll(List<? super T> products) {
        products.forEach(System.out::println);
    }
}
