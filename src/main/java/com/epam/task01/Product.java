package com.epam.task01;

public abstract class Product {
    protected String name;
    protected int price;
    protected int releaseYear;

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public Product(String name, int price, int releaseYear) {
        this.name = name;
        this.price = price;
        this.releaseYear = releaseYear;
    }
}
