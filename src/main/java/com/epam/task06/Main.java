package com.epam.task06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<StringComparable> stringCompatibles = new ArrayList<>();
        stringCompatibles.add(new StringComparable("Ukraine", "Kyiv"));
        stringCompatibles.add(new StringComparable("Germany", "Berlin"));
        stringCompatibles.add(new StringComparable("USA", "Washington"));
        Collections.sort(stringCompatibles);
        stringCompatibles.forEach(System.out::println);

        List<StringComparator> stringComparators = new ArrayList<>();
        stringComparators.add(new StringComparator("Ukraine", "Kyiv"));
        stringComparators.add(new StringComparator("Germany", "Berlin"));
        stringComparators.add(new StringComparator("USA", "Washington"));
        int index = Collections.binarySearch(
                stringComparators,
                new StringComparator("USA", "Washington"),
                new StringComparator());
        System.out.println(stringComparators.get(index));
        stringComparators.forEach(System.out::println);
    }
}
