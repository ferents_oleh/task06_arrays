package com.epam.task06;

public class StringComparable implements Comparable<StringComparable> {
    private String first;

    private String second;

    public StringComparable(String first, String second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public int compareTo(StringComparable o) {
        return Integer.compare(first.length(), o.first.length());
    }

    @Override
    public String toString() {
        return first;
    }
}
