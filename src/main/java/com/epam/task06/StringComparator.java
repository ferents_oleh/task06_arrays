package com.epam.task06;

import java.util.Comparator;

public class StringComparator implements Comparator<StringComparator> {
    private String first;

    private String second;

    public StringComparator() {}

    public StringComparator(String first, String second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public int compare(StringComparator o1, StringComparator o2) {
        return Integer.compare(o1.second.length(), o2.second.length());
    }

    @Override
    public String toString() {
        return second;
    }
}
